package com.example.demo.controller;

import com.example.demo.model.Student;
import com.example.demo.repo.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentController {
    @Autowired
    private StudentRepo studentRepo;
    @GetMapping("/api/v1/students")
    public List<Student> getAllStudent(){
        return studentRepo.findAll();
    }
    @GetMapping("/api/v1/student/{id}")
    public Student getStudentById(@PathVariable long id){
        return studentRepo.findById(id).orElse(new Student());
    }
    @PostMapping("/api/v1/student")
    public Student insertStudent(@RequestBody Student student){
        return studentRepo.save(student);
    }
    @GetMapping("/api/v1/student/hello")
    public String insertStudent(){
        return "Hello student";
    }

}
